import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SocketmoduleModule } from './socketmodule/socketmodule.module';
import { ScheduleModule } from '@nestjs/schedule';
import config from './config/config';
@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(config.mongoURI),
    SocketmoduleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [ScheduleModule],
})
export class AppModule {}
