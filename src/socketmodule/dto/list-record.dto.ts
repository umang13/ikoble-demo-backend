import { ApiPropertyOptional } from '@nestjs/swagger';
import { OrderByEnum } from '../order.enum';
export class ListRecordDto {
  @ApiPropertyOptional({
    description: 'limit',
    example: 10,
  })
  limit: number;

  @ApiPropertyOptional({
    description: 'skip',
    example: 1,
  })
  skip: number;

  @ApiPropertyOptional({
    description: 'order',
    example: 'ASC',
  })
  order_by: OrderByEnum;

  @ApiPropertyOptional({
    description: 'value',
    example: 'type1',
  })
  text: string;
}
