import * as mongoose from 'mongoose';

export const UniqueSchema = new mongoose.Schema({
  unique_str: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  timestamp: { type: Date, default: Date.now },
});
