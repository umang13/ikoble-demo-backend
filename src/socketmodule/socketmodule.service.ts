import { ListRecordDto } from './dto/list-record.dto';
import { Unique } from './interface/uniquestring.interface';
import {
  Injectable,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import e = require('express');
import List from './interface/list.interface';

const randomstring = require('randomstring');
const typeArray = ['type1', 'type2', 'type3'];

@Injectable()
@WebSocketGateway()
export class SocketmoduleService
  implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    @InjectModel('Unique')
    private readonly UniqueStringModel: Model<Unique>,
  ) {}

  //Web Socket Server init
  @WebSocketServer() server;

  //Cron Created To generated Record per second
  @Cron('* * * * * *')
  async handleCron() {
    const data = new this.UniqueStringModel();
    data.unique_str = randomstring.generate(16);
    data.type = typeArray[0];
    typeArray.push(typeArray[0]);
    typeArray.shift();

    try {
      //Save Data to collection
      const result = await data.save();

      const response = {
        data: result.type,
        message: 'Succesfully inserted',
      };

      await this.handleConnection(data);
    } catch (error) {
      return error.message;
    }
  }

  async getRecord(paginationOption: ListRecordDto): Promise<List> {
    try {
      const { limit, skip, order_by, text } = paginationOption;

      let obj = {
        uStr: 'unique_str',
        tpe: 'type',
      };

      let order: number;

      let search_value: string;
      if (order_by && order_by.toUpperCase() == 'ASC') {
        order = 1;
      } else if (order_by && order_by.toUpperCase() == 'DESC') order = -1;
      let data;

      if (text) {
        data = await this.UniqueStringModel.find({
          $or: [
            {
              [obj.uStr]: { $regex: `.*${paginationOption.text}.*` },
            },

            {
              [obj.tpe]: { $regex: `.*${paginationOption.text}.*` },
            },
          ],
        })
          .limit(+limit || 1000)
          .skip(+skip)
          .sort({ timestamp: order || -1 })
          .exec();
      } else {
        data = await this.UniqueStringModel.find()
          .limit(+limit || 1000)
          .skip(+skip)
          .sort({ timestamp: order || -1 })
          .exec();
      }

      if (!data || data.length === 0) {
        throw new NotFoundException(`No data found`);
      }
      let totalRecordLength = await this.UniqueStringModel.find().countDocuments();
      const response = {
        total: data.length,
        all_record: totalRecordLength,
        record_details: data,
      };

      return response;
    } catch (error) {
      throw new InternalServerErrorException(
        `Oops.. Something went wrong, please try again later`,
      );
    }
  }

  async handleConnection(data) {
    // Push Generated String to Frontend
    try {
      this.server.emit('strrec', data);
    } catch (error) {
      throw new InternalServerErrorException(
        `Oops.something went wrong, please try again later`,
      );
    }
  }

  async handleDisconnect() {}
}
