import { Test, TestingModule } from '@nestjs/testing';
import { SocketmoduleModule } from './socketmodule.module';

describe('Socketmodule Controller', () => {
  let controller: SocketmoduleModule;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SocketmoduleModule],
    }).compile();

    controller = module.get<SocketmoduleModule>(SocketmoduleModule);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
