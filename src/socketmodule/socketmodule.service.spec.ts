import { Test, TestingModule } from '@nestjs/testing';
import { SocketmoduleModule } from './socketmodule.module';

describe('SocketmoduleService', () => {
  let service: SocketmoduleModule;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SocketmoduleModule],
    }).compile();

    service = module.get<SocketmoduleModule>(SocketmoduleModule);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
