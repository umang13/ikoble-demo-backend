import * as mongoose from 'mongoose';

export interface Unique extends mongoose.Document {
  unique_str?: String;
  type?: String;
}
