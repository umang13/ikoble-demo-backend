export default interface List{
    total:number;
    record_details:ListItem [];
}

interface ListItem {
    _id: string,
    timestamp: Date,
    unique_str: string,
    type: string,
    __v: number
}