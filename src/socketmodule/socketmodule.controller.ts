import { ListRecordDto } from './dto/list-record.dto';
import { Controller, Get, Query, BadRequestException } from '@nestjs/common';
import { SocketmoduleService } from './socketmodule.service';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import List from './interface/list.interface';

@ApiTags('Socket')
@Controller('socket')
export class SocketmoduleController {
  constructor(private socketService: SocketmoduleService) {}

  @ApiTags('Socket')
  @Get('/list')
  @ApiResponse({ status: 200, description: 'Api success' })
  @ApiResponse({ status: 404, description: 'No record found' })
  @ApiResponse({ status: 422, description: 'Bad Request or API error message' })
  
  async getRecord(@Query() paginationOption: ListRecordDto): Promise<List> {

    const { order_by } = paginationOption;

    if (
      order_by &&
      order_by.toUpperCase() != 'ASC' &&
      order_by.toUpperCase() != 'DESC'
    ) {
      throw new BadRequestException(`${order_by} is not a valid order by `);
    }

    return this.socketService.getRecord(paginationOption);
  }
}
