import { Module } from '@nestjs/common';
import { SocketmoduleController } from './socketmodule.controller';
import { SocketmoduleService } from './socketmodule.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UniqueSchema } from './schema/uniquestring.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Unique', schema: UniqueSchema }]),
  ],
  controllers: [SocketmoduleController],
  providers: [SocketmoduleService],
})
export class SocketmoduleModule {}
